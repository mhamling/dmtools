import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render () {
    return (
      <div>
        <h1>Hello, adventurer!</h1>
            <p>Welcome to your new Dungeons and Dragons tool <a href='/Identity/Account/Register?returnUrl=/authentication/login'>Click Here</a> to get started!</p>
            <p>If you already have an account feel free to <a href='/authentication/login'>Log In!</a></p>
            <p>DmTools is your helper to help you with your campaigns. It's not nessarly an online session, but rather an augmentation to D&D gameplay.</p>
      </div>
    );
  }
}
