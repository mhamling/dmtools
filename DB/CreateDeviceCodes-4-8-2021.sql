USE [DMTools]
GO

/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 4/6/2021 12:47:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DeviceCodes](
	[UserCode] [nvarchar](200) NOT NULL,
	[DeviceCode] [nvarchar](200) NOT NULL,
	[SubjectId] [nvarchar](200) NULL,
	[SessionId] [nvarchar](100) NULL,
	[ClientId] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[CreateTime] [datetime2] NOT NULL,
	[Expiration] [datetime2] NOT NULL,
	[Data] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.DeviceCodes] PRIMARY KEY CLUSTERED 
(
	[USerCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[DeviceCodes] ADD  DEFAULT (getdate()) FOR [CreateTime]
GO


